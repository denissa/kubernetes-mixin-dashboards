
##### Manual build dashboards
```bash
brew install jsonnet
mkdir -p dashboards

jb init
jb install github.com/kubernetes-monitoring/kubernetes-mixin

##### Build grafanaDashboards
jsonnet -J vendor -m dashboards -e '(import "mixin.libsonnet").grafanaDashboards'

##### Build record rules
jsonnet -J vendor -S -e 'std.manifestYamlDoc((import "mixin.libsonnet").prometheusRules)' | yq eval -P > ./rules/rules.yaml

##### Build alerts rules
jsonnet -J vendor -S -e 'std.manifestYamlDoc((import "mixin.libsonnet").prometheusAlerts)' | yq eval -P > ./rules/alerts.yaml