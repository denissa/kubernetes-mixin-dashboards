local kubernetes = import "kubernetes-mixin/mixin.libsonnet";

kubernetes {
  _config+:: {
   kubeApiserverSelector: 'job="apiserver"',
   cadvisorSelector: 'job="kubelet"',
   kubeletSelector: 'job="kubelet"',
   showMultiCluster: true,
  },
}